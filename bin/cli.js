#! /usr/bin/env node
/**
 * #! /usr/bin/env node是用git Bash模拟unix （必须)
 * 运行指令（tpc-cli自定义改）：npm unlink && npm link && tpl-cli
 */
// -----------------------------------------------------------------
/**
 * node.js 命令行解决方案
 * https://github.com/tj/commander.js/blob/HEAD/Readme_zh-CN.md
 */
const commander = require('commander')
/**
 * 用户与命令行交互的工具
 * https://www.npmjs.com/package/inquirer
 */
const inquirer = require('inquirer')
const { name, version } = require('../package.json')
const { create } = require('../lib/create.js')
/**
 *  _         _            _ _
 *  | |_ _ __ | |       ___| (_)
 *  | __| '_ \| |_____ / __| | |
 *  | |_| |_) | |_____| (__| | |
 *   \__| .__/|_|      \___|_|_|
 *      |_|
 * https://www.npmjs.com/package/figlet
 * @type {me|{}}
 */
const figlet = require('figlet')

/**
 * 询问用户信息
 * prompt 用户信息列表 示例
 * https://github.com/SBoudrias/Inquirer.js/tree/master/packages/inquirer/examples
 * @param args 指令
 * @param options
 * @param nextFunction 下一步操作
 */
function inquirerFunc (cmd, options, nextFunction) {
  // console.log('输入Cmd：', cmd, '输入Options：', options)
  inquirer.prompt([
    {
      type: 'input', // type： input, number, confirm, list, checkbox ...
      name: 'name', // key 名
      message: 'Please input project name?', // 提示信息
      default: cmd // 默认值
    },
    // {
    //   type: 'input',
    //   name: 'desp',
    //   message: 'Please input the project description?'
    // },
    {
      type: 'list', // list 选择
      name: 'tpl',
      message: 'Please choose a template',
      choices: ['tpl-v2', 'tpl-v3']
    }
  ]).then((answers) => {
    // 输入结果，执行下一步运行
    nextFunction(answers, options)
  })
}

/**
 * 入口
 * Start cli (create)
 */
(async () => {
  console.log(figlet.textSync(name))
  commander
    .version(version)
    .command('create <name>')
    .description('create a new project')
    .option('-f, --force', 'overwrite target directory if it exist')
    .action((cmd, options) => {
      inquirerFunc(cmd, options, create)
    })

  commander.parse()
})()
