# 自定义脚手架

## 项目配置
```
npm install
```

### 开发环境编译及热重载
```
node --trace-warnings ./bin/cli.js create test
```

### 生产环境编译打包
```
npm unlink && npm link && tpl-cli create test
```

### 代码检查及纠正
```
npm run lint
```

### npm发包
```
npm publish
```

### 使用（创建）
```
(@X)tpl-cli create <name> 
```
