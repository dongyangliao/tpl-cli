const $const = require('../const/index.js')
const baseUrl = $const.BASE_URL
const baseAccount = $const.BASE_ACCOUNT
const fs = require('fs')
// const download = require("download-git-repo")
const Git = require('nodegit')
/**
 * ora >6 不支持 require
 */
const ora = require('ora')

const cloneOptions = {}
cloneOptions.fetchOpts = {
  callbacks: {
    certificateCheck: function () { return 0 }
  }
}

class Generator {
  constructor (name, tpl, pwd) {
    this.name = name
    this.tpl = tpl
    this.pwd = pwd
    // console.log(this.name, this.tpl, this.pwd);
  }

  async getRepository () {
    const spinner = ora('Process fetch repository...').start()
    spinner.color = 'blue';
    spinner.start()
    const cloneURL = `${baseUrl}/${baseAccount}/${this.tpl}.git`
    const repo = await Git.Clone(cloneURL, this.pwd, cloneOptions)
    spinner.text = 'download success / 下载成功'
    spinner.color = 'green'
    spinner.succeed()
    return repo;
    // const url = `${baseUrl}/${this.tpl}`;
    // await download(url, this.pwd, (error) => {
    //     if (error) {
    //         console.log('error statusMessage: ', error.statusMessage)
    //         spinner.color = 'red'
    //         spinner.text = 'download.failed / 当前模板无权限'
    //         spinner.fail()
    //     } else {
    //         spinner.color = 'green'
    //         spinner.text = 'download success / 下载成功'
    //         spinner.succeed()
    //     }
    // })
  }

  setPackageJsonConfig() {
    const packageConfig = require(`../config/${this.tpl}.js`);
    packageConfig.name = this.name;
    let VanillaStr = JSON.stringify(packageConfig, null, "\t");
    fs.writeFileSync(`${this.pwd}\\package.json`, VanillaStr, { 'flag': 'w' }, function (err) {
      if (err) {
        console.log(err);
      }
    })
  }

  async create () {
    await this.getRepository()
    // console.log(repo)
    this.setPackageJsonConfig();
    console.log('Done')
  }
}

module.exports = {
  Generator
}
