const path = require('path')
const { Generator } = require('./generator.js')
/**
 * fs-extra 是对 fs 模块的扩展，支持 promise 语法
 */
const fs = require('fs-extra')

const inquirer = require('inquirer')

function pwdResolve (dir) {
  return path.join(process.cwd(), dir)
}

const removePwd = async (dir) => {
  await fs.remove(dir)
}

const createProject = (name, tpl, pwd) => {
  const generator = new Generator(name, tpl, pwd)
  generator.create()
}

/**
 * 创建项目
 * @param answers
 */
const create = async (answers, options) => {
  const { name, tpl } = answers
  console.log('create function answers/回答信息:', name, tpl)
  const pwd = pwdResolve(name)
  // console.log('print work directory/工作目录:', pwd)
  const { force } = options
  // folder exists
  if (fs.existsSync(pwd)) {
    if (force) {
      await removePwd(pwd)
      createProject(name, tpl, pwd)
    } else {
      const { action } = await inquirer.prompt([{
        name: 'action',
        type: 'list',
        message: 'Target directory already exists Please pick an action:',
        choices: ['overwrite', 'No']
      }])
      if (!action) {
        return null
      } else if (action === 'overwrite') {
        // 移除已存在的目录, 确定要覆盖
        await removePwd(pwd)
        createProject(name, tpl, pwd)
      }
    }
  } else {
    createProject(name, tpl, pwd)
  }
}

module.exports = {
  create
}
